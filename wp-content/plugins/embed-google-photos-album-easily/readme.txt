=== Embed Google Photos album ===
Contributors: pavex
Donate link: https://www.publicalbum.org/blog/about-pavex
Tags: Google Photos, Embed Google Photos, Wordpress carousel, Carousel slideshow, Embed gallery
Tested up to: 5.4
Requires PHP: 5.3
License: GPLv2 or later

Embed Google Photos album using Player widget.

== Description ==

	If you want to embed a Google Photos album on your site, you can use this plugin to insert an album using the "Player" or Carousel slideshow widget.
This widget, unsupported some features like a picture's timestamp and captions. For a more detailed description of the component, go to [Wordpress Google Photos album plugin](https://www.publicalbum.org/blog/wordpress-google-photos-album-plugin) or another post with update details.
This widget is free to use for Wordpress users.

== Example ==

`[embed-google-photos-album link="https://photos.app.goo.gl/CSV7NDstShTUwUZq5"]`

`[embed-google-photos-album link="https://photos.app.goo.gl/CSV7NDstShTUwUZq5" mode="carousel"]`

`[embed-google-photos-album link="https://photos.app.goo.gl/CSV7NDstShTUwUZq5" mediaitems-cover="true"]`

- **link** - [string] public link of Google Photos album
- **mode** - [carousel | gallery-player] setup decorator mode, default id `gallery-player`
- **width** - [int | 'auto'] set widget width in pixel or "auto" to stretch to 100%
- **height** - [int | 'auto'] set widget height in pixels or "auto" to stretch to 100%
- **image-width** - [int] image max-width in pixels, default is 1920
- **image-height** - [int] image max-height in pixels, default is 1080
- **autoplay** - [true | false] start slideshow in normal view (currently not allowed by decorator)
- **delay** - [true | false] slideshow delay in seconds, default is 5 seconds.
- **repeat** - [true | false] Enable or disable repeat slideshow, delfault is `true`
- **mediaitems-aspectration** - [true | false], Keep asspect ration of images delfault is `true`
- **mediaitems-enlarge** - [true | false], Turn on/off image enlarge, delfault is `true`
- **mediaitems-stretch** - [true | false], Tunr on/off image stretch, delfault is `true`
- **mediaitems-cover** - [true | false], Cover full canvas. Combine with aspect ratio parameter. Delfault is `false`
- **expiration** - [int] setup expiration timeout in secons; default is 0; min. custom value is 86400s (experimental property)

In some case it may be better used directly in templates with following code:

`<?php
	echo (new Pavex_embed_google_photos_album()) -> getcode(
		'https://photos.app.goo.gl/CSV7NDstShTUwUZq5', 0, 480, 1920, 1080
	);
?>
`


== How do I update my album? ==

The album will update automatically as soon as you save or update your post.


== Changelog ==

= 2.1.1 =
*Release Date - 24 April 2020*

* repair expiration property
* Worpress 5.4 test 

= 2.1.0 =
*Release Date - 12 July 2019*

* use embed-ui.min.js decorator
* Carousel or Gallery player support
* slideshow parameters shorten names
* mediaitems parameters for image setup
* function getcode() accept complex props

= 2.0.9 =
*Release Date - 4 June 2019*

* load javascript in the footer

= 2.0.8 =
*Release Date - 2 June 2019*

* set_transient expiration support

= 2.0.7 =
*Release Date - 10 December 2018*
 
* getcode() for templates inline php

= 2.0.6 =
*Release Date - 21 November 2018*
 
* wp_remote_get

= 2.0.5 =
*Release Date - 15 November 2018*
 
* fix to use the latest version of decorator

= 2.0.4 =
*Release Date - 10 November 2018*
 
* using decorator from cdn.jsdelivr.net service

= 2.0.3 =
*Release Date - 9 November 2018*
 
* fix bug with widget size.
* set widget dimensions to int or 'auto' to stretch to parent element size
* remove stretch

= 2.0.2 =
*Release Date - 25 October 2018*
 
* rename and add new properties to setup slideshow. 

= 2.0.1 =
*Release Date - 19 August 2018*
 
* imageWidth/imageHeight options to set better quality of images. 

= 2.0.0 =
*Release Date - 9 August 2018*

* Based on simple grabber of the album. Not backward compatibility with previous versions.

= 0.9 =

* Proxy of Publicalbum.org embeds iframe.
