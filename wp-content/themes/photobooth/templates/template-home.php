<?php
/*
 * Template Name: Home Template
*/
?>

<?php get_header('home'); ?>

<main id="site-content">
	<?php get_template_part('template-parts/section', 'home');?>
</main>

<?php get_footer();?>
