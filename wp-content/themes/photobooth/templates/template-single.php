<?php
/*
 * Template Name: Single Page Template
 * Template Post Type: page
*/
?>
<?php get_header(); ?>
<div class="stretch-to-full-page">
	<main id="site-content">
		<?php
		global $post;
		get_template_part('template-parts/section', 'content');
		$post_slug = $post->post_name;
		get_template_part('page', $post_slug);
		wp_reset_postdata();
		?>
	</main>
	<?php get_footer(); ?>
</div>