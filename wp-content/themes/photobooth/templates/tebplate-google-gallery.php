<?php
/*
 * Template Name: Google photos Gallery Template
*/
?>

<?php get_header(); ?>
<div class="stretch-to-full-page">
    <div class="google-gallery-page-wrapper">
        <div class="google-gallery-wrapper">
            <?php
                echo do_shortcode('[embed-google-photos-album
                    link="https://photos.app.goo.gl/FsfYzoHPFVLfpXzY6"
                    width="auto"
                    height="auto"
                    slideshow-autoplay="true"
                    slideshow-repeat="true"
                ]');
            ?>
        </div>
    </div>
	<?php get_footer(); ?>
</div>

