<div class="container-fluid">
    <?php
    $events = get_posts(array(
        'numberposts' => -1,
        'post_type'   => 'events_cpt',
        'meta_key'            => 'order',
        'order'                => 'ASC'
    ));

    if ($events) :
        foreach ($events as $index => $event) :
            $event_id = $event->ID;
            $event_heading     = get_field('event_heading', $event_id);
            $event_description = get_field('event_description', $event_id);
            $addon_image_url   = get_field('event_image', $event_id)['url'];
            $event_button_link = get_field('event_button_link', $event_id);
            $event_side_label  = get_field('event_side_label', $event_id);
            $gallery           = acf_photo_gallery('event_gallery', $event_id);
    ?>
            <!--py-3 py-lg-4-->
            <div class="event-row row pb-2 py-sm-4 mt-3 mt-sm-4 py-md-5 py-xl-5">
                <div class="col-12">
                    <div class="event-side-label-container d-flex align-items-center justify-content-center">
                        <div class="position-relative event-side-label-container-inner">
                            <p class="event-side-label"><?php echo $event_side_label; ?></p>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row d-sm-block event-content-container no-gutters">
                            <div class="col-12">
                                <?php $carousel_id = 'events_carousel' . $event_id; ?>
                                <?php $carousel_id_selector = '#' . $carousel_id; ?>
                                <div id="<?php echo $carousel_id ?>" class="carousel-container carousel slide carousel-fade" data-ride="carousel" data-interval="false">
                                    <div class="carousel-inner">
                                        <?php
                                        if (count($gallery)) :
                                            foreach ($gallery
                                                as $img_index => $image) :
                                                $full_image_url = $image['full_image_url'];
                                                $class = $img_index === 0 ? 'carousel-item active' : 'carousel-item';
                                        ?>
                                                <div class="<?php echo $class; ?>">
                                                    <img src="<?php echo $full_image_url; ?>" class="img-fluid" alt="Carousel Image">
                                                </div>
                                        <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </div>
                                    <a class="carousel-control-prev" href="<?php echo $carousel_id_selector ?>" role="button" data-slide="prev">
                                        <div class="slider-arrow-container-previous" aria-hidden="true">
                                            <img src="" alt="Previous">
                                        </div>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="<?php echo $carousel_id_selector ?>" role="button" data-slide="next">
                                        <div class="slider-arrow-container-next" aria-hidden="true">
                                            <img src="" alt="Next">
                                        </div>

                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <div class="row event-carousel-details align-items-center mx-auto">
                                    <div class="col-12 col-md-5 col-lg-4 offset-xl-1">
                                        <h2 class="event-heading text-md-left <?php if (strlen($event_heading) > 15) : echo 'long-heading';
                                                                                endif; ?>">
                                            <?php echo $event_heading; ?>
                                        </h2>
                                        <a href="<?php echo $event_button_link; ?>" target="_blank" class="btn event-book-now-btn event-book-now-btn-crsl d-none d-md-block">Book
                                            now</a>
                                    </div>
                                    <div class="col-12 col-md-7 col-lg-8 col-xl-7 justify-content-start">
                                        <p class="event-description text-left">
                                            <?php echo $event_description; ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-12 d-inline-block btn-book-now-events-mobile">
                                    <a href="<?php echo $event_button_link; ?>" target="_blank" class="d-md-none btn event-book-now-btn text-center">Book
                                        now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <?php endforeach;
    endif; ?>
    <?php
    get_template_part('template-parts/section', 'locations');
    get_template_part('template-parts/section', 'quotes');
    ?>

</div>