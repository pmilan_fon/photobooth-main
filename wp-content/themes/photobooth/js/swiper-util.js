jQuery(document).ready(function(){
    initAllSwipers();
});

function initAllSwipers() {
    var heroSwiper = new Swiper('.swiper-hero', {
        centeredSlides: true,
        direction: 'vertical',
        loop: true,
        slidesPerView: 1,
        effect: "coverflow",
        spaceBetween: 1000,
        autoplay: {
            delay: 3000,
        },
    });
    swiperPackages();
    swiperServices();
    swiperOurCustomers();
}

function swiperServices() {

    function isHome () {
        return jQuery('body.home').length ? 1 :2 ;
    }

    var $serviceSwiper = new Swiper('.swiper-services', {

        slidesPerColumnFill: 'row',
        slidesPerView: 1.175,
        slidesPerGroup: 2,
        slidesPerColumn: isHome(),
        slidesPerColumnFill: 2,
        slidesPerGroupSkip: 2000,
        initialSlide: 0,
        slideToClickedSlide: true,

        breakpoints: {
            480: {
                slidesPerView: 1.25,
                slidesPerGroup: 2,
                slidesPerColumn: 2,
                allowTouchMove: true,
            },

            576: {
                slidesPerView: 1.35,
                slidesPerGroup: 2,
                slidesPerColumn: 2,
                allowTouchMove: true,
            },

            768: {
                slidesPerView: 2.35,
                slidesPerGroup: 3,
                slidesPerColumn: 2,
                allowTouchMove: true,
            },

            992: {
                slidesPerView: 3.5,
                slidesPerGroup: 3,
                slidesPerColumn: 2,
                allowTouchMove: true,
            },

            1024: {
                slidesPerView: 4,
                slidesPerGroup: 2,
                slidesPerColumn: 2,
                allowTouchMove: false,
            }
        }

    })
}

function swiperOurCustomers() {
    return new Swiper('.swiper-our-customers', {
        slidesPerView: 2.25,
        slidesPerColumn: 2,
        slidesPerColumnFill: 'row',
        freeMode: true,
        freeModeMomentumRatio: 0.5,
        freeModeMomentumVelocityRatio: 0.5,
        freeModeMomentumBounceRatio: 0.5,
        shortSwipes: false,
        grabCursor: true,
        setWrapperSize: true,
        breakpoints: {
            992: {
                slidesPerColumn: 1,
                slidesPerView: 3.25,

            },

            1200: {
                slidesPerColumn: 1,
                slidesPerView: 3.75,
            }
        }
    });

}

function swiperPackages() {
    var printBoothSwiper = new Swiper('.swiper-packages-printbooth', {
        initialSlide: 1,
        centeredSlides: true,
        slideToClickedSlide: true,
        grabCursor: true,
        spaceBetween: 20,
        breakpoints: {
            // when window width is >= 320px
            0: {
                slidesPerView: 1.15,
                spaceBetween: 10
            },

            320: {
                slidesPerView: 1.25,
                spaceBetween: 10
            },

            375: {
                slidesPerView: 1.4,
                spaceBetween: 10
            },

            425: {
                slidesPerView: 1.45,
                spaceBetween: 13
            },

            494: {
                slidesPerView: 1.55,
                spaceBetween: 13
            },

            576: {
                slidesPerView: 1.65,
                spaceBetween: 15
            },
            768: {
                slidesPerView: 2.25,
                spaceBetween: 20
            }
            // when window width is >= 640px
        }
    });

    var swiperChewy = new Swiper('.swiper-packages-chewybooth', {
        initialSlide: 0,
        spaceBetween: 23,
        centeredSlides: false,
        init: true,
        slideToClickedSlide: true,
        grabCursor: true,
        breakpoints: {
            // when window width is >= 320px
            0: {
                slidesPerView: 1.15,
                spaceBetween: 10
            },

            375: {
                slidesPerView: 1.35,
                spaceBetween: 23
            },
            425: {
                slidesPerView: 1.5,
                spaceBetween: 23
            }
        }
    });

}


