jQuery(document).ready(function($) {
    const templateUrl = param.templateUrl;
    const arrow_path = "/assets/booth-slider-arrow.svg";

    $('.nextend-arrow-previous').addClass('slider-arrow-container-previous');
    $('.nextend-arrow-next').addClass('slider-arrow-container-next');
    $('.slider-arrow-container-previous, .slider-arrow-container-next')
        .children('img').attr('src', templateUrl + arrow_path);

    $('.bigfoot-menu-toggler').on("click", function (event) {
        $('.bigfoot-menu-toggler-icon').toggleClass('rotate');
        $('.bigfoot-menu-toggler-icon').toggleClass('rotate-reset');
    });

});


