jQuery(document).ready(function($) {
    var pathname = window.location.pathname;
    var url      = window.location.href;
    var baseUrl = 'http://localhost';
    // var baseUrl = 'https://photobooth.webdevelopmentbelgrade.com/';

    function isExpanded() {
        var aria = $('.bigfoot-menu-toggler').attr('aria-expanded');
        if (aria === 'true') {
            return true;
        } else {
            return false;
        }
    };
    
    if (pathname === '/wordpress/' || baseUrl === url) {
        $('.bigfoot-menu-toggler').click(function () {
            if (isExpanded() === true) {
                var scroll = $(window).scrollTop();
                if (scroll < 70) {
                    $('.navbar-expand-lg').addClass( "transparent" );
                    $('.navbar-expand-lg').removeClass( "white" );
                }
            } else {
                $('.navbar-expand-lg').addClass( "white" );
                $('.navbar-expand-lg').removeClass( "transparent" );
            }
        });
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 50) {
                $('.navbar-expand-lg').addClass( "white" );
                $('.navbar-expand-lg').removeClass( "transparent" );
            } else {
                if (isExpanded() === false) {
                    $('.navbar-expand-lg').addClass( "transparent" );
                    $('.navbar-expand-lg').removeClass( "white" );
                }
            }
        });
    } else {
        $('.navbar-expand-lg').addClass( "white" );
    }

});