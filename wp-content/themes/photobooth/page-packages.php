<div id="packages" class="pb-packages">

	<?php
    get_template_part('template-parts/section', 'services');
	get_template_part( 'template-parts/section', 'addons' );
    get_template_part( 'template-parts/section', 'packages' );
    get_template_part( 'template-parts/section', 'locations' );
    ?>

</div>