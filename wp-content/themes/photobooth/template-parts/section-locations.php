<section id="locations">
    <div class="container">
        <div class="locations-wrapper d-flex align-items-center">
            <?php
                $args = array( 'post_type' => 'locations',
                'post_status' => 'publish',
                'orderby' => 'date',
                'order'   => 'ASC');
                $wp_query = new WP_Query($args);
                while ($wp_query->have_posts()) {
                    $wp_query->the_post();
                    $location_name = get_field( "location_name", get_the_ID() );
                    $location_map_url = get_field( "location_map_url", get_the_ID() );
                    $address = get_field( "address", get_the_ID() );
                    $image = get_field('hover_image', get_the_ID() );
                    $image_url = $image['url'];
                    ?>
                    <div class='location'>
                        <div class="image-wrapper">
                            <img src="<?php echo $image_url ?>">
                        </div>
                        <div class='white-box'>
                            <p class='location-city'><?php echo $location_name; ?></p>
                        </div>
                    </div>
                    <?php
                }
            ?>
        </div>
    </div>
</section>