<?php
include 'section-packages-helper.php';
define( 'PACKAGES_PAGE_ID', 146 );

$print_booth_hashtag_text = get_field( 'print_booth_hashtag_text', PACKAGES_PAGE_ID );
$print_booth_heading      = get_field( 'print_booth_heading', PACKAGES_PAGE_ID );
$print_booth_button_url   = get_field( 'get_custom_quote_url', PACKAGES_PAGE_ID );
$print_booth_packages     = get_field( 'pricing_packages_print_booth', PACKAGES_PAGE_ID )['package_item'];
$chewy_booth_heading      = get_field( 'chewy_booth_heading', PACKAGES_PAGE_ID );
$chewy_booth_subheading   = get_field( 'chewy_booth_sub_heading', PACKAGES_PAGE_ID );
$chewy_booth_packages     = get_field( 'pricing_packages_chewy_booth', PACKAGES_PAGE_ID )['package_item'];

?>
<section id="pricing_packages" class="pricing">
    <div class="container">
        <div class="row">
            <div class="col-12">
				<?php
				$page_slug = basename( get_permalink() );
				if ( $page_slug === 'packages' ) :
					echo '<h3 class="pkg-subheading-printbooth">Packages for weddings and regular events</h3>';
				else:
					echo '<h1 class="pricing-heading pkg-heading-printbooth justify-content-center">OUR PACKAGES</h1>';
				endif;
				?>
            </div>
        </div>
        <div class="row align-items-center position-relative">
            <div class="col-lg-9">
                <h1 class="packages-photobooths-hash"><?php echo $print_booth_hashtag_text; ?></h1>
                <h2 class="pricing-heading pkg-type-printbooth printbooth text-left">PRINT BOOTH</h2>
            </div>
			<?php if ( is_front_page() ) { ?>
                <a class="gradient-btn d-lg-block d-none"
                   href="<?php echo get_field( 'get_custom_quotes_url', '119' ) ?>">
                    <button class="get-a-custom-quote-btn">
                        <p class="upper">Corporate events</p>
                        <p class="bottom">get a custom quote</p>
                    </button>
                </a>
			<?php } ?>

        </div>
        <!--Print booth packages-->
        <div class="row main-pkg-container-printbooth d-none d-lg-flex packages-container-printbooth justify-content-center justify-content-lg-between align-items-start">
			<?php

			if ( $print_booth_packages ) {
				foreach ( $print_booth_packages as $package ) {
					$package_data = Package::mapFromAcfPackage( $package ); ?>
					<?php echo create_package_card( $package_data->package_type, $package_data->package_class, $package_data->package_name,
						$package_data->package_hours, $package_data->package_price, $package_data->package_offers, $package_data->package_url );
					?>
					<?php
				}
			}
			?>
        </div>
        <div class="row main-pkg-container-printbooth d-flex d-lg-none justify-content-center">
            <!--Swiper-->
            <div class="col-12 position-relative  p-0">
                <div class="swiper-container swiper-packages-printbooth swiper-overflow-visible">
                    <div class="swiper-wrapper">
						<?php
						if ( $print_booth_packages ) {
							foreach (
								$print_booth_packages
								as $package
							) {
								$package_data = Package::mapFromAcfPackage( $package ); ?>
								<?php echo create_package_card_swiper( $package_data->package_type, $package_data->package_class, $package_data->package_name,
									$package_data->package_hours, $package_data->package_price, $package_data->package_offers, $package_data->package_url );
								?>
								<?php
							}
						}
						?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-4 mb-2">
            <a class="gradient-btn d-block d-lg-none" href="<?php echo get_field( 'get_custom_quotes_url', '119' ) ?>">
                <button class="get-a-custom-quote-btn">
                    <p class="upper">Corporate events</p>
                    <p class="bottom">get a custom quote</p>
                </button>
            </a>
        </div>
        <!--Chewy booth packages-->
        <div class="row packages-container-chewy justify-content-between align-items-center position-relative">
            <div class="col-12 col-md-4">
				<?php wp_reset_query();
				$page_slug = basename( get_permalink() );
				if ( $page_slug == 'packages' ) :?>
                    <p class="custom-branding-offer d-none d-sm-block">Choice of any booth</p>
				<?php endif; ?>
                <h2 class="pricing-heading pkg-heading-chewybooth text-left chewy-description"><?php echo $chewy_booth_heading; ?></h2>
                <h5 class="custom-branding-offer-slim"><?php echo $chewy_booth_subheading; ?></h5>
            </div>
            <div class="col-12 col-md-8 col-lg-7">
                <!-- >= 576px-->
                <div class="row justify-content-between d-none d-sm-flex">
					<?php
					if ( $chewy_booth_packages ) {
						foreach ( $chewy_booth_packages as $package ) {
							$package_data = Package::mapFromAcfPackage( $package ); ?>
							<?php echo create_package_card( $package_data->package_type, $package_data->package_class, $package_data->package_name,
								$package_data->package_hours, $package_data->package_price, $package_data->package_offers, $package_data->package_url ); ?>
							<?php
						}
					} ?>
                </div>

                <!--<=576px-->
                <div class="row d-flex d-sm-none">
                    <div class="swiper-container swiper-packages-chewybooth">
                        <div class="swiper-wrapper">
							<?php
							if ( $chewy_booth_packages ) {
								foreach ( $chewy_booth_packages as $package ) {
									$package_data = Package::mapFromAcfPackage( $package ); ?>
									<?php echo create_package_card_swiper( $package_data->package_type, $package_data->package_class, $package_data->package_name,
										$package_data->package_hours, $package_data->package_price, $package_data->package_offers, $package_data->package_url ); ?>
									<?php
								}
							} ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-1 px-0 d-none d-lg-flex ">
                <img class="digital-hash-img" src="<?php echo get_template_directory_uri() . '/assets/digital.svg'; ?>" alt="Digital">
            </div>
        </div>
    </div>
</section>

<?php
wp_reset_query();
$page_slug = basename( get_permalink() );
if ( $page_slug == 'packages' ) :
	include( locate_template( 'template-parts/block-corporate-events.php', false, false ) );
endif; ?>
