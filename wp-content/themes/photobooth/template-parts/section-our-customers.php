<?php define('OUR_CUSTOMERS_PAGE_ID', '458'); ?>
<section class="pb-our-customers">
    <div class="container our-customers">
        <div class="our-customers-header">
            <h1 id="our-customers-hashtag">#INSTAGRAM</h1>
            <h2 id="our-customers-title">Our Customers Love Us</h2>
        </div>
        <div class="swiper-container swiper-our-customers swiper-overflow-visible">
            <div class="swiper-wrapper">
                <?php
                $gallery = acf_photo_gallery('gallery', OUR_CUSTOMERS_PAGE_ID);
                $instagram_link = get_field('instagram_page_url', OUR_CUSTOMERS_PAGE_ID);
                if (count($gallery)) :
                    foreach ($gallery as $img_index => $image) :
                        $full_image_url = $image['full_image_url'];
                        echo "<div class='swiper-slide imageSlide'>";
                        $thumb_image_url = acf_photo_gallery_resize_image($full_image_url, 280, 280);
                        echo
                            "<div class='swiper-img-content'>" .
                            "<img id='swiper-img-item' class='img-fluid' src=" . $thumb_image_url .
                            "></div></div>";
                    endforeach; endif;
                ?>
            </div>
        </div>
        <div class="our-customers-instagram">
            <div class="instagram-items d-flex justify-content-center">
                <a class="gradient-btn gradient-btn-thick-border follow-us" target="_blank"
                   href="<?php echo $instagram_link ?>">
                    <button class="d-flex align-items-center">
                        <div class="instagram-logo"
                             style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/Images/Instagram.svg');"></div>
                        <div class="instagram-logo-white" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/Images/Instagram-white.svg');"></div>
                        <p>Follow us</p>
                    </button>
                </a>
            </div>
        </div>
    </div>
</section>

