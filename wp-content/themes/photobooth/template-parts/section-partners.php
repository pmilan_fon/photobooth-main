<section id="partners">
    <div class="container">
        <div class="col-12">
            <div class="row">
                <?php
                $partners = get_posts( array(
	                'numberposts' => - 1,
	                'post_type'   => 'partners'

                ) );
		        if ( $partners ) {
			        foreach ( $partners as $partner ) {
				        $partner_id = $partner->ID;
				        $image_url  = get_field( 'partner_image', $partner_id )['url'];
				        $order_class_small_device = 'order-' . get_field('order_mobile_tablet', $partner_id);
				        $order_class_large_device = 'order-lg-' . get_field('order_desktop', $partner_id);
				        $order_classes = $order_class_small_device . " " . $order_class_large_device; ?>

                        <div class="partner-image-container col-4 col-md-3 col-xl-2 <?php echo $order_classes;?>">
                            <img src="<?php echo $image_url; ?>" alt="Partner Image">
                        </div>
				        <?php
			        }
		        }
		        ?></div>
        </div>
    </div>
</section>
