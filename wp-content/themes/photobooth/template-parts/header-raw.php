<?php
$dark_logo     = get_template_directory_uri() . '/assets/Landing/bfhead-dark.png';
$white_logo     = get_template_directory_uri() . '/assets/Landing/bfhead.png';
$page_template = get_page_template_slug();
if ($page_template == 'templates/template-home.php') :
    $navbar_style = 'bigfoot-navbar-home';
else :
    $navbar_style = 'bigfoot-navbar-single';
endif;
?>

<section class="header">
    <nav class="navbar navbar-expand-lg py-3 navbar-bigfoot <?php echo $navbar_style; ?>">
        <div class="container">
            <a href="<?php echo home_url(); ?>" class="d-flex navbar-brand">
                <img class="dark-logo" src="<?php echo $dark_logo ?>" alt="Bigfoot Photobooth">
                <img class="white-logo" src="<?php echo $white_logo ?>" alt="Bigfoot Photobooth">
            </a>
            <button class="bigfoot-menu-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#bigfootCollapsingNavbar" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </button>

            <div class="collapse navbar-collapse align-items-baseline" id="bigfootCollapsingNavbar">
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'primary',
                        'container'      => true,
                        'menu_class'     => 'primary-nav navbar-nav text-center justify-content-center'
                    )
                );
                ?>
                <a class="contact-us-btn contact-mobile" href="<?php echo get_permalink('497'); ?>">
                    <div class="button-border">
                        <button>Contact us</button>
                    </div>
                </a>
            </div>
            <a class="contact-us-btn contact-desktop" href="<?php echo get_permalink('497'); ?>">
                <div class="button-border">
                    <button>Contact us</button>
                </div>
            </a>
        </div>
    </nav>
</section>