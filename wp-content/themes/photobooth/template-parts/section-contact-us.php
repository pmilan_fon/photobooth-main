<?php define( 'CONTACT_US_PAGE_ID', '497' );

$page_slug = basename( get_permalink() );
if( $page_slug == 'contact-us') :
    $side_image_style = 'side-hashtag-image';
else:
    $side_image_style = 'side-hashtag-image-left';
endif;
$contact_us_field_group = get_field( 'contact_us_field_group', CONTACT_US_PAGE_ID );
$side_image_url         = $contact_us_field_group['side_image']['url'];
$heading_text           = $contact_us_field_group['heading_text'];
$phone_number           = $contact_us_field_group['phone_number'];
$form_shortcode         = $contact_us_field_group['form_shortcode'];
?>
<section id="contact-us" class="position-relative">
    <img class="<?php echo $side_image_style; ?>" src="<?php echo $side_image_url; ?>"
         alt="#Contact">
    <!--</div>-->
    <div class="container contact-form-container position-relative">
        <div class="row align-items-center">
            <div class="col-md-5 col-lg-4 col-xl-3 offset-lg-1">
                <div class="d-inline-block  justify-content-center">
                    <h1 class="contact-us-heading"><?php echo $heading_text; ?></h1>
                    <div class="phone-text justify-content-between  d-flex d-md-inline-block">
                        <span>Or contact us at <br class="d-none d-md-block"/></span>
                        <span class="phone-number"><img src="<?php echo get_template_directory_uri() . '/assets/phone-fill.svg' ?>" class="phone-image" alt="Contact"><?php echo $phone_number; ?></span></div>
                </div>


            </div>
            <div class="col-md-6 col-xl-5 offset-md-1 align-items-center">
				<?php echo do_shortcode( $form_shortcode ); ?>
            </div>
        </div>

    </div>
</section>
