<section id="faq-section" class="faq pb-faq">
    <div class="container bg-transparent">
        <div class="d-flex faq-content">
            <div class="content-left">
                <p id="question" class="text-nowrap">
                    HAVE A question?
                </p>
                <p id="p-faq">FAQ
                    <span id="s-in-faq">s</span>
                </p>
            </div>
            <span id="rotateArrow" class="faq-btn " type="button" data-toggle="collapse"
               data-target="#collapseFaq">
                <img id="arrow-img" draggable="false" class="faq-btn-img normal" onclick="Rotate()"
                     src="<?php echo get_template_directory_uri() . '/assets/FAQ/arrow-right.svg' ?>">
            </span>
        </div>
        <div class="collapse-container">
            <div class="collapse" id="collapseFaq">
                <div class="row">
                    <?php
                    $faqs_posts = array('post_type' => 'faqs',
                        'posts_per_page' => -1,
                        'post_status' => 'publish',
                        'orderby' => 'date',
                        'order' => 'ASC');
                    $wp_query = new WP_Query($faqs_posts);
                    while ($wp_query->have_posts()) {
                        $wp_query->the_post();
                        $question = get_field("question", get_the_ID());
                        $answer = get_field("answer", get_the_ID());
                        ?>
                        <div class="col-md-6 col-12 faq-item">
                            <h1><?php echo $question ?></h1>
                            <p><?php echo $answer ?></p>
                        </div>

                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>

        <script>
            var rotate = false;

            function Rotate() {

                if (rotate == true) {
                    document.getElementById('arrow-img').className = 'normal';
                    rotate = false;
                } else {
                    document.getElementById('arrow-img').className = 'rotate';
                    rotate = true;
                }
            }
        </script>
    </div>
</section>