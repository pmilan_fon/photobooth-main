<?php $hero = get_field( 'hero_section' ); ?>
<section class="hero-section">
    <div class="side-hashtag">
        <img src="<?php echo $hero['hero_side_hashtag_image']['url']; ?>" alt="#Photobooth">
    </div>
    <div class="container pt-3 pb-4 pb-lg-3">
        <div class="row justify-content-between">
            <div class="col-12 col-md-5 col-lg-5 hero-content-main">
                <p class="text-nowrap big-foot">BIG FOOT</p>
                <div class="hero-text">
					<?php echo $hero['hero_text'] ?>
                </div>
                <div class="py-4 d-inline-block hero-btn-wrapper">
                    <a class="d-block btn book-now" target="_blank" href="<?php echo $hero['hero_button_link'] ?>">Book Now</a>
                    <span class="d-block text-nowrap text-center d-inline-block mt-4 "><small
                                class="have-a-question pl-2 text-center">Have a question?</small><a
                                href="<?php $url = get_option( 'siteurl' ); echo $url; ?>/contact-us/" class="ml-1 contact-us-link">Contact us</a></span>
                </div>
            </div>
            <div class="d-none d-md-flex col-md-7 col-lg-7 mt-md-0 mt-sm-4 hero-image-content-container">
                <div class="swiper-container swiper-hero">
                    <div class="swiper-wrapper">
						<?php
						$hero_content_posts = get_posts( array(
                            'numberposts' => - 1,
                            'orderby' => 'date',
                            'order'   => 'ASC',
							'post_type'   => 'hero_contents'
						) );

						if ( $hero_content_posts ) {
							foreach ( $hero_content_posts as $hero_content ) {
								$post_type_id       = $hero_content->ID;
								$hero_content_field = get_field( 'hero', $post_type_id );

								$image_url        = $hero_content_field['hero_image']['url'];
								$caption_top      = $hero_content_field['hero_image_hashtag_caption_top'];
								$caption_bottom_1 = $hero_content_field['hero_image_hashtag_caption_bottom_1'];
								$caption_bottom_2 = $hero_content_field['hero_image_hashtag_caption_bottom_2']; ?>

                                <div class="swiper-slide">
                                    <figure class="image-figure mb-0 mx-3">
                                        <figcaption
                                                class="m-0 p-0 small-hashtag-top"><?php echo $caption_top; ?></figcaption>
                                        <img class="img-fluid hero-image" src="<?php echo $image_url ?>"
                                             alt="hero_image"/>
                                        <figure class="hero-hash-captions" style="vertical-align: top">
                                            <figcaption
                                                    class="hash-main"><?php echo $caption_bottom_1 ?></figcaption>
                                            <figcaption
                                                    class="hash-sub"><?php echo $caption_bottom_2; ?></figcaption>
                                        </figure>
                                    </figure>
                                </div>
								<?php
							}
						}
						?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>