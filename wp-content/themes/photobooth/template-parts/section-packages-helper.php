<?php
class Package {

	public $package_class;
	public $package_name;
	public $package_hours;
	public $package_price;
	public $package_offers;
	public $package_type;
	public $package_url;

	/**
	 * Package constructor.
	 *
	 * @param $package_type
	 * @param $package_class
	 * @param $package_name
	 * @param $package_hours
	 * @param $package_price
	 * @param $package_offers
	 * @param $package_url
	 */
	public function __construct( $package_type, $package_class, $package_name, $package_hours, $package_price, $package_offers, $package_url ) {
		$this->package_type   	= $package_type;
		$this->package_class 	= $package_class;
		$this->package_name   	= $package_name;
		$this->package_hours  	= $package_hours;
		$this->package_price  	= $package_price;
		$this->package_offers	= $package_offers;
		$this->package_url		= $package_url;
	}

	static function mapFromAcfPackage( $package ) {
		$package_id = $package->ID;

		$package_name       = get_field( 'package_name', $package_id );
		$package_hours     	= get_field( 'package_hours', $package_id );
		$package_price      = get_field( 'package_price', $package_id );
		$package_offers     = get_field( 'package_offers', $package_id );
		$package_popularity = get_field( 'is_popular', $package_id );
		$package_url		= get_field( 'book_now_button_link', $package_id );
		$package_css_class  = $package_popularity === true ? 'package-highlighted' : 'package';
		$package_type       = get_the_terms( $package_id, 'package_type' )[0]->slug;

		return new Package( $package_type, $package_css_class, $package_name, $package_hours, $package_price, $package_offers, $package_url );
	}

}

function create_package_card( $package_type, $package_class, $package_name, $package_hours, $package_price, $package_offers, $package_url ) {
	ob_start();
	$css_class               = implode( ' ', array( $package_type, $package_class ) );
	$isPrintBooth            = strcmp( $package_type, 'print-booth' ) == 0;
	$isChewyBooth            = strcmp( $package_type, 'chewy-booth' ) == 0;
	$isPrintBoothHighlighted = $isPrintBooth && strcmp( $package_class, 'package-highlighted' ) == 0;
	$package_column_class    = $isPrintBooth ?
		'offset-lg-0 col-lg-4 py-2' :
		'col-12 col-sm-6 col-md-6 py-3';
	?>

    <div class="<?php echo $package_column_class;
	if ( ! $isPrintBoothHighlighted ) : echo ' mt-lg-3'; endif; ?>">
        <div class="<?php echo $css_class; ?>">
            <div class="package-item">
				<?php if ( ! empty( $package_name ) ) : ?>
                <h4 class="name"><?php echo $package_name;
					endif; ?></h4>

                <div class="package-details">
					<?php
					if ( $isPrintBoothHighlighted  || $isChewyBooth) : ?>
						<?php if ( ! empty( $package_price ) ) : ?>
                            <h5 class="price"><?php echo $package_price;
						endif; ?></h5>
						<?php if ( ! empty( $package_hours ) ) : ?>
                            <h5 class="hours"><?php echo $package_hours;
						endif; ?></h5>
					<?php else: ?>
						<?php if ( ! empty( $package_hours ) ) : ?>
                            <h5 class="hours"><?php echo $package_hours;
						endif; ?></h5>
						<?php if ( ! empty( $package_price ) ) : ?>
                            <h5 class="price"><?php echo $package_price;
						endif; ?></h5>
					<?php endif; ?>
                </div>

				<?php if ( ! empty( $package_offers ) ) : ?>
                <div class="offers">
					<?php echo $package_offers;
					endif; ?>
                </div>

				<?php
				if ( $isPrintBoothHighlighted ) : ?>
                    <img src="<?php echo get_template_directory_uri() . '/assets/Packages/MOSTPOPULAR.svg' ?>"
                         class="side-label" alt="Most popular">
				<?php endif; ?>

                <a href="<?php echo $package_url; ?>" class="btn bn-btn">Book Now</a>
            </div>
        </div>
    </div>
	<?php
	return ob_get_clean();
}

function create_package_card_swiper( $package_type, $package_class, $package_name, $package_hours, $package_price, $package_offers, $package_link ) {
	ob_start();
	$css_class                           = implode( ' ', array( $package_type, $package_class ) );
	$is_print_booth_highglighted_package = strcmp( $package_type, 'print-booth' ) == 0
	                                       && strcmp( $package_class, 'package-highlighted' ) == 0;

	$isChewyBooth            = strcmp( $package_type, 'chewy-booth' ) == 0;

	if ($isChewyBooth) {
		$padding_vertical_class  = 'py-1';
    } else {
        if ($is_print_booth_highglighted_package) {
            $padding_vertical_class = 'pt-0';
        } else {
	        $padding_vertical_class = 'pt-2';
        }
    }
	?>

    <div class="swiper-slide align-items-start <?php echo $padding_vertical_class;?>">
        <div class="<?php echo $css_class; ?>">
            <div class="package-item  overlay-max">
				<?php if ( ! empty( $package_name ) ) : ?>
                <h4 class="name"><?php echo $package_name;
					endif; ?></h4>

                <div class="package-details">
					<?php
					if ( $is_print_booth_highglighted_package || $isChewyBooth ) : ?>
						<?php if ( ! empty( $package_price ) ) : ?>
                            <h5 class="price"><?php echo $package_price;
						endif; ?></h5>
						<?php if ( ! empty( $package_hours ) ) : ?>
                            <h5 class="hours"><?php echo $package_hours;
						endif; ?></h5>
					<?php else: ?>
						<?php if ( ! empty( $package_hours ) ) : ?>
                            <h5 class="hours"><?php echo $package_hours;
						endif; ?></h5>
						<?php if ( ! empty( $package_price ) ) : ?>
                            <h5 class="price"><?php echo $package_price;
						endif; ?></h5>
					<?php endif; ?>
                </div>

				<?php if ( ! empty( $package_offers ) ) : ?>
                <div class="offers">
					<?php echo $package_offers;
					endif; ?>
                </div>

				<?php
				if ( $is_print_booth_highglighted_package ) : ?>
                    <img src="<?php echo get_template_directory_uri() . '/assets/Packages/MOSTPOPULAR.svg' ?>"
                         class="side-label" alt="Most popular">
				<?php endif; ?>

                <a target="_blank" href="<?php echo $package_link; ?>" class="btn bn-btn">Book Now</a>
            </div>
        </div>
    </div>
	<?php
	return ob_get_clean();
}

?>

