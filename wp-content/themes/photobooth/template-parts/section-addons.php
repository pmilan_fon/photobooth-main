<section id="addons" class="position-relative">
    <div class="side-label-container">
        <p class="addon-side-label">#Add Ons</p>
    </div>
    <div id="add-ons" class="container">
        <h1 class="add-ons-heading">Add ons</h1>

        <div class="row align-items-end justify-content-center">
			<?php
			$args     = array(
				'post_type'   => 'addons',
				'post_status' => 'publish',
				'orderby'     => 'date',
				'order'       => 'ASC'
			);
			$wp_query = new WP_Query( $args );
			while ( $wp_query->have_posts() ) {
				$wp_query->the_post();
				$addon_id          = get_the_ID();
				$addon_name        = get_field( 'addon_name', $addon_id );
				$addon_description = get_field( 'addon_description', $addon_id );
				$addon_image_url   = get_field( 'addon_image', $addon_id )['url'];
				?>
                <div class="col-12 col-md-10 col-lg-6 py-1 addon">
                    <div class="addon-card-container">
                        <div class="col-12 col-sm-8 addon-card align-items-center">
                            <div class="row justify-content-center"><img src="<?php echo $addon_image_url; ?>"
                                                                         class="addon-image img-fluid"
                                                                         alt="Addon image"></div>
                            <div class="row justify-content-center">
                                <h1 class="addon-name"><?php echo $addon_name; ?></h1>
                            </div>
                            <div class="row justify-content-center">
                                <h3 class="addon-description"><?php echo $addon_description; ?></h3>
                            </div>
                        </div>
                    </div>
                </div>
				<?php
			}
			wp_reset_query();
			?>
        </div>
    </div>
</section>