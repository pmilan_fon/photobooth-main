<div class="container-fluid all-booths-container">
	<?php
	$booths = get_posts( array(
		'numberposts' => - 1,
		'post_type'   => 'booth',
		'meta_key'			=> 'order',
		'order'				=> 'ASC'
	) );

	if ( $booths ) :
		foreach ( $booths as $index => $booth ) :
			$booth_id = $booth->ID;
			$booth_name                   = get_field( 'booth_name', $booth_id );
			$booth_description            = get_field( 'booth_description', $booth_id );
			$book_this_booth_button_link  = get_field( 'book_this_booth_button_link', $booth_id );
			$booth_image_slider_shortcode = get_field( 'booth_image_slider_shortcode', $booth_id );
			$booth_label_image_url        = get_field( 'booth_side_label_image', $booth_id )['url'];
			?>
            <div class="row booth-type position-relative">
				<?php ( $index % 2 == 0 ) ? $change_order_class = "order-md-1" : $change_order_class = ""; ?>
                <div class="booth-vertical-caption">
                    <img src="<?php echo $booth_label_image_url ?>" class="booth-label-image" alt="Image label">
                </div>
                <div class="container booth-container">
                    <div class="row align-items-center justify-content-between">
                        <div class="booth-description-container col-12 col-md-5 col-lg-4 order-2 <?php echo $change_order_class; ?>">
                            <h1 class="booth-name mt-2"><?php echo $booth_name; ?></h1>
                            <p class="booth-description"><?php echo $booth_description; ?>
                            </p>
                            <a target="_blank" href="<?php echo $book_this_booth_button_link?>" class="btn bigfoot-btn">Book This Booth</a>
                        </div>
                        <div class="col-12 col-md-7 col-lg-7 order-1 ">
                            <div class="booth-slider-container">
	                            <?php echo do_shortcode( $booth_image_slider_shortcode ); ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
		<?php endforeach; endif; ?>
</div>