<?php $about_us = get_field( 'about_us' ); ?>
<section id="section-about-us">
    <div class="container">
        <div class="col-xs-12 pb-5 pt-3">
            <div class="about-us-box">
                <div class="about-us-box-content">
                    <h2 class="about_us_heading"><?php echo $about_us['title'] ?></h2>
					<?php echo $about_us['text'] ?>
                </div>
            </div>
        </div>
    </div>
</section>
