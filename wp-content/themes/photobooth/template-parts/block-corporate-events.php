<div class="container corporate-custom-quotes-block">
	<div class="col-12 col-sm-10 col-lg-8 corporate-events-box py-4 mx-auto">
		<div class="row justify-content-around py-4">
			<div class="col-9 col-md-6 col-lg-6 col-xl-5">
				<h1 class="corporate-events-block-heading">Corporate events?</h1>
			</div>
		</div>
		<div class="row pb-3">
			<a href="<?php echo get_field('get_custom_quotes_url', '119') ?>" class="corporate-events-block-btn text-center">
				Get a qustom quote
			</a>
		</div>
	</div>
</div>