<section class="pb-services">
    <div class="container services">
        <div class="services-hashtag">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/Images/services-hashtag.svg">
        </div>
        <?php $services = get_field('services_item'); ?>
        <h1 id="service-title">Services</h1>
        <div>
            <div id="service-swiper">
                <div class="swiper-services swiper-container swiper-overflow-visible">
                    <div class="swiper-wrapper">
                        <?php
                        wp_reset_query();
                        $items = is_front_page() ? 7 : 8;
                        $service_posts = array('post_type' => 'services',
                            'post_status' => 'publish',
                            'orderby' => 'date',
                            'order' => 'ASC');
                        $wp_query = new WP_Query($service_posts);
                        $i= 0;
                        while (($wp_query->have_posts()) && ($i < $items)) {
                            $wp_query->the_post();
                            $service_name = get_field( "service_name");
                            $service_description = get_field( "service_description");
                            $service_icon = get_field( "service_icon")['url'];
                            ?>
                            <div class="service-item swiper-slide">
                                <div class="service-image-container">
                                    <img class="img-fluid service-img" src="<?php echo $service_icon; ?>"
                                         alt="service image">
                                </div>
                                <div class="service-title-container">
                                    <h2><?php echo $service_name; ?></h2>
                                </div>
                                <p><?php echo $service_description; ?></p>
                            </div>
                        <?php
                            $i++;}
                            ?>
                        <?php
                        wp_reset_query();
                        if (is_front_page()) { ?>
                            <div class="service-item swiper-slide">
                                <div class="service-image-container">
                                    <img class="img-fluid service-img" src='<?php echo get_template_directory_uri(); ?>/assets/Images/addons-icon.svg');
                                         alt="service image">
                                </div>
                                <div class="service-title-container">
                                    <h2>And many more add ons</h2>
                                </div>
                                <div class="eight-item-main">
                                    <h3 class="service-link">Check all add-ons here</h3>
                                    <a class="service-link-btn"
                                       href="packages#add-ons">
                                        <img class="img-fluid service-link-img"
                                             src="<?php echo get_template_directory_uri() . '/assets/Services/services_link_button.svg' ?>"
                                             alt="Arrow">
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
</section>