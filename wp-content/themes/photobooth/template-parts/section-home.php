<div id="hero" class="pb-hero position-relative pt-0">

	<?php
	include( locate_template( 'template-parts/header-raw.php', false, false ) );
	?>
	<?php get_template_part( 'template-parts/section', 'hero' ); ?>
</div>

<div id="partners">
	<?php get_template_part( 'template-parts/section', 'partners' ); ?>
</div>

<div id="about-us" class="pb-about-us">
	<?php get_template_part( 'template-parts/section', 'about-us' ); ?>
</div>

<div id="our-booths" class="pb-our-booths pt-0 pb-2">
	<?php get_template_part( 'template-parts/section', 'booths' ); ?>
    <!--Turn grid into carousel
	https://stackoverflow.com/questions/34444731/turn-responsive-grids-in-carousel-when-750p-->

</div>

<div id="packages" class="pb-packages">
	<?php get_template_part( 'template-parts/section', 'packages' ); ?>
</div>

<div id="socials" class="pb-socials">
    <?php get_template_part( 'template-parts/section', 'our-customers');?>
</div>
<div id="services">
	<?php get_template_part( 'template-parts/section', 'services' ); ?>
</div>
<div id="contact" class="pb-contact-form">
	<?php get_template_part( 'template-parts/section', 'contact-us' ); ?>
</div>
<div id="location-maps" class="pb-location-maps">
	<?php get_template_part( 'template-parts/section', 'locations' ); ?>
</div>
<div id="quotes" class="pb-custom-quotes">
	<?php get_template_part( 'template-parts/section', 'quotes' ); ?>
</div>


