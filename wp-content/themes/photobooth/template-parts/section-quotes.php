<?php $quotes = get_field( 'custom_quotes', 119 );?>

<section id="section-quotes">
    <div class="container p-0 quotes-container row justify-content-center">
        <div class="col-12 col-sm-10 col-md-9 col-lg-8">
            <div class="quotes-box">
                <div class="quotes-box-content">
                    <h2 class="quotes_heading"><?php echo $quotes['title'] ?></h2>
                    <p><?php echo $quotes['text'] ?></p>
                    <a href="<?php echo get_field('get_custom_quotes_url', 119);?>" id="quotes-btn" type="button" class="btn btn-light">
                        <span id="quotes-link-text" class="text-nowrap"><?php echo $quotes['link_text'] ?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>



