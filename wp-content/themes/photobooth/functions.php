<?php
/*
	==========================================
	 Include scripts
	==========================================
*/
function debug_to_console($data)
{
    $output = $data;
    if (is_array($output))
        $output = implode(',', $output);

}

// Needed to remove default 32px margin top from html element
function my_function_admin_bar()
{
    return false;
}

add_filter('show_admin_bar', 'my_function_admin_bar');

add_theme_support('menus');

function photobooth_custom_header_setup() {
	$args = array(
		'default-text-color' => '000',
		'flex-width'         => true,
		'flex-height'        => true,
	);
    add_theme_support( 'custom-header', $args );
}
add_action( 'after_setup_theme', 'photobooth_custom_header_setup' );


function photobooth_script_enqueue()
{
    $template_directory_uri = get_template_directory_uri();

    // CSS
    wp_enqueue_style('bootstrap', $template_directory_uri . '/node_modules/bootstrap/dist/css/bootstrap.css', array(), '4.4.1', 'all');
    wp_enqueue_style('customstyle', $template_directory_uri . '/css/photobooth-main.css', array(), '1.0.0', 'all');
    wp_enqueue_style('swiper', $template_directory_uri. '/node_modules/swiper/css/swiper.css', array(), '5.3.8', 'all');

	/*3rd party JS dependencies*/
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/node_modules/bootstrap/dist/js/bootstrap.js', array(), '4.4.1', true);
	wp_enqueue_script('swiperjs', $template_directory_uri . '/node_modules/swiper/js/swiper.js', array(), '5.3.8', true);
	wp_enqueue_script('swiper-util', $template_directory_uri . '/js/swiper-util.js', array(), '1.0.0', true);
	wp_enqueue_script('gallery-dependency', 'https://photoboothtalk.com/gallery/assets/gallery-embed.js', array(), '1.0.0', true);

	/*Custom JS files*/
    wp_enqueue_script('header-menu', $template_directory_uri . '/js/header-menu.js', array(), '1.0.0', true);
    wp_enqueue_script('slider-button-replacer', $template_directory_uri . '/js/slider-button-replacer.js');
	$params = array( 'templateUrl' => get_template_directory_uri() );
    wp_localize_script( 'slider-button-replacer', 'param', $params );

}

add_action('wp_enqueue_scripts', 'photobooth_script_enqueue');

/*
	==========================================
	==========================================
*/

function photobooth_theme_setup()
{

    add_theme_support('menus');

    register_nav_menu('primary', 'Primary Header Navigation');
    /*  register_nav_menu('secondary', 'Footer Navigation');*/

}

add_action('init', 'photobooth_theme_setup');

add_theme_support('custom-background');
add_theme_support('post-thumbnails');
add_theme_support('post-formats',array('aside','image','video'));
/*
	==========================================
	 Theme support function
	==========================================

*/
/*
add_theme_support('custom-background');
add_theme_support('custom-header');
add_theme_support('post-thumbnails');
add_theme_support('post-formats',array('aside','image','video'));*/

