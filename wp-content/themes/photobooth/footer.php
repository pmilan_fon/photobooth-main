<footer class="pb-footer">

	<?php
    wp_footer();
	// TODO: Kad se ubace sve stranice:
	// Lista page ideva za koje ne treba faq, i onda if pre ovog-a
	include( locate_template( 'template-parts/section-faqs.php', false, false ) ); ?>
    <section id="footer">
        <div class="container align-items-center">
            <div class="row justify-content-center justify-content-lg-end py-3 py-md-2">
                <p class="pb-footer-text"> 2020 © Bigfoot Photobooths. All rights reserved. Design by Coreware
                    Group. </p>
            </div>
        </div>
    </section>

</footer>

</body>
</html>